package steps;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import pages.BasePage;

public interface ActionSteps extends CommonSteps {

  @Step("Открыть страницу {url}")
  default <T extends BasePage & CommonSteps> T openPage(String url){
    Selenide.open(url);
    return (T) this;
  }
}
