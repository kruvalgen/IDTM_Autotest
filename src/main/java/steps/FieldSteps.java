package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import pages.BasePage;

import static pages.BasePage.getInputField;
import static testContext.ContextHolder.replaceVarsIfPresent;


public interface FieldSteps extends CommonSteps {

  @Step("в поле {string} ввести значение {string}")
  default <T extends BasePage & CommonSteps> T enterValueInInputField(String fieldName, String value){
    value = replaceVarsIfPresent(value);
    SelenideElement field = getInputField(fieldName)
        .shouldBe(Condition.visible
            .because("Поле " + fieldName + " не найдено на странице"));
    field.setValue(value);
    return (T) this;
  }
}
