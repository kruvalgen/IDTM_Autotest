package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import pages.BasePage;

import static pages.BasePage.getButton;


public interface ButtonSteps extends CommonSteps{

  @Step("Нажать на кнопку {string}")
  default <T extends BasePage & CommonSteps> T clickOnButton (String buttonName){
    SelenideElement button = getButton(buttonName)
        .shouldBe(Condition.visible
            .because("Кнопка " + buttonName + " не найдена на странице"));
    button.click();
    return (T) this;
  }
}
