package hooks;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;

public class Hooks {


  @BeforeAll
  static void setUp(){
//    SelenideLogger.addListener("allure", new AllureSelenide());
  }

  @AfterAll
  static void tearDown(){
    Selenide.closeWebDriver();

  }


}
