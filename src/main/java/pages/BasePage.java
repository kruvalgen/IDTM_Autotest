package pages;

import com.codeborne.selenide.SelenideElement;
import steps.ActionSteps;
import steps.ButtonSteps;
import steps.CommonSteps;
import steps.FieldSteps;

import static com.codeborne.selenide.Selenide.$x;

public abstract class BasePage implements ActionSteps, ButtonSteps, FieldSteps {

  //---------------------------[locators]-----------------------------------------------------------
  public static SelenideElement getInputField(String fieldName) {
    return $x(String.format("//input[@placeholder='%1$s']", fieldName));
}

  public static SelenideElement getButton(String buttonName){
    return $x(String.format("//div[@role='button']//span[text()='%1$s'] " +
        "| //button[./span[normalize-space()='%1$s']]", buttonName));
  }

  //---------------------------[methods]------------------------------------------------------------


}
