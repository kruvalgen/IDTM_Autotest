import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.*;
import pages.AuthorizationPage;
import pages.MainPage;

public class Tests {

  public AuthorizationPage authorizationPage = new AuthorizationPage();
  @DisplayName ("Тестовый сценарий")
  @Test
  void testScenario(){
    authorizationPage
        .openPage("http://192.168.77.220/")
        .enterValueInInputField("Логин", "Autotest1")
        .enterValueInInputField("Пароль", "1");
    MainPage mainPage = authorizationPage.goToMainPage();
    mainPage.clickOnButton("Создать документ");
  }

}
